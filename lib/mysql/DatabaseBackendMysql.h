/**
 * @file DatabaseBackendMysql.h
 */
#ifndef DATABASEBACKENDMYSQL_H_
#define DATABASEBACKENDMYSQL_H_

#include <DBAPI.h>
#include <DatabaseBackend.h>
#include <string>
#include <mysql/mysql.h>

class DBAPI::DatabaseBackendMySQL : public DBAPI::DatabaseBackend {
public:
	DatabaseBackendMySQL(std::string dsn);

	void setQuery(std::string sql);
	void setQuery(char* sql);
	std::string escape(std::string value);
	char* escape(char* value);
	bool execute();
	DBAPI::ResultSet* getResultSet();
	std::string getError();
	int getErrorNum();

private:
	std::string _query;
	std::string _dsn;
	MYSQL *_conn;
	MYSQL_RES *_result;
	int _errorNum;
	std::string _errorStr;
};

#endif /* DATABASEBACKENDMYSQL_H_ */
