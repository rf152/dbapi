#include "DatabaseBackendMysql.h"

#include <ParamParser.h>
#include <stdio.h>
#include <sstream>
#include <GenericException.h>
#include <ResultSet.h>
#include <Result.h>
#include <MetaValue.h>
#include <map>

//TODO: Remove this
#include <iostream>
using std::cout;
using std::endl;

using namespace DBAPI;
using std::string;
using std::stringstream;
using std::map;
using std::pair;

extern "C" {
	DBAPI::DatabaseBackend* _DatabaseEntry(const char *dsn) {
		string dsn2 = dsn;
		return new DatabaseBackendMySQL(dsn2);
	}
}

DatabaseBackendMySQL::DatabaseBackendMySQL(string dsn) {
	ParamParser *params = new ParamParser();
	params->parse(dsn);
	string host, sport, database, user, password;
	int port;
	MYSQL *result;

	host = params->getParam("host", "localhost");
	sport = params->getParam("port", "3306");
	database = params->getParam("database", "");
	user = params->getParam("user", "");
	password = params->getParam("password", "");
	
	cout << "Params (mysql): " << endl <<
			" Host: " << host << endl <<
			" Port: " << sport << endl <<
			" DB: " << database << endl <<
			" User: " << user << endl <<
			" Pass: " << password << endl;

	stringstream(sport) >> port;

	result = _conn = mysql_init(NULL);
	if (!result) {
		//an error occured
		cout << "Mysql init failed" << endl;
		throw GenericException("mysql_init failed");
		return;
	}
	result = mysql_real_connect(_conn,
			host.c_str(),			//Host to connect to
			user.c_str(),			//Username
			password.c_str(),		//Password
			database.c_str(),		//Database specified
			port,					//Port to connect to
			NULL,					//unix_socket - not supported
			0);						//client flag
	if (!result) {
		//an error occured
		cout << "Could not connect to the server" << endl;
		cout << mysql_error(_conn) << endl;
		throw GenericException("Could not connect to server");
		return;
	}
}

void DatabaseBackendMySQL::setQuery(std::string sql) {
	_query = sql;
}
void DatabaseBackendMySQL::setQuery(char* sql) {
	_query = sql;
}
std::string DatabaseBackendMySQL::escape(std::string value) {
	cout << "Definitely escaping" << endl;
	char *escaped = new char[value.length()*3 + 1];
	int result;
	cout << this->_conn << endl;
	result = mysql_real_escape_string(this->_conn, escaped, value.c_str(), value.length());
	cout << result << endl;
	if (!result) {
		return "";
	}
	return escaped;
}
char* DatabaseBackendMySQL::escape(char* value) {
	const char* escaped;
	escaped = escape(string(value)).c_str();
	return (char*)escaped;
}
bool DatabaseBackendMySQL::execute() {
	if (_query == "") return true;
	int result;

	result = mysql_real_query(_conn, _query.c_str(), _query.length());
	if (result) {
		cout << "ERROR! " << mysql_error(_conn);
		return false;
	}
	_result = mysql_store_result(_conn);
	return true;
}
ResultSet* DatabaseBackendMySQL::getResultSet() {
	ResultSet *rs;
	Result *r;
	MetaValue *m;

	int i, j, k;
	MYSQL_ROW row;
	MYSQL_FIELD *field;
	map<int, int> fieldTypes;

	double vd;
	bool vb;
	int vi;
	char *vc;

	rs = new ResultSet();
	cout << "ResultSet Size: " << mysql_num_rows(_result) << endl;
	rs->_results.resize(mysql_num_rows(_result));
	if (mysql_num_rows(_result) == 0) {
		return rs;
	}

	j = mysql_num_fields(_result);

	for (i=0; i<j; i++) {
		field = mysql_fetch_field(_result);
		fieldTypes.insert(pair<int,int>(i, field->type));
	}
	k = 0;
	while((row = mysql_fetch_row(_result))) {
		r = new Result();
		for (i=0; i<j; i++) {
			switch (fieldTypes.at(i)) {
				case 0:
				case 4:
				case 5:
				case 246:
					stringstream(row[i]) >> vd;
					r->addValue(new MetaValue(vd));
					break;

				case 1:
				case 2:
				case 3:
				case 7:
				case 8:
				case 9:
				case 13:
					stringstream(row[i]) >> vi;
					r->addValue(new MetaValue(vi));
					break;

				case 10:
				case 11:
				case 14:
				case 15:
				case 16:
				case 247:
				case 248:
				case 249:
				case 250:
				case 251:
				case 252:
				case 253:
				case 254:
				case 255:
				default:
					vc = (char*)row[i];
					r->addValue(new MetaValue(vc));
					break;
			}
		}
		rs->_results.at(k) = r;
		k++;
	}
	return rs;

}
string DatabaseBackendMySQL::getError() {
	return string(mysql_error(_conn));
}
int DatabaseBackendMySQL::getErrorNum() {
	return mysql_errno(_conn);
}
