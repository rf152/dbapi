/**
 * @file ColumnOutOfBoundsException.h
 */
#ifndef COLUMNOUTOFBOUNDSEXCEPTION_H_
#define COLUMNOUTOFBOUNDSEXCEPTION_H_

#include "DBAPI.h"
#include "GenericException.h"

/**
 * @brief Exception for accessing a column that is out of bounds
 */
class DBAPI::ColumnOutOfBoundsException: public DBAPI::GenericException {
public:
	ColumnOutOfBoundsException(int colID);
};

#endif /* COLUMNOUTOFBOUNDSEXCEPTION_H_ */
