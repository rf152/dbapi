#include "ColumnOutOfBoundsException.h"
#include <stdio.h>

using DBAPI::ColumnOutOfBoundsException;

ColumnOutOfBoundsException::ColumnOutOfBoundsException(int colID) {
	char* desc;
	sprintf(desc, "Column %d Out of Bounds", colID);
	_description = desc;
}
