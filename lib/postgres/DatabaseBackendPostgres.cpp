#include "DatabaseBackendPostgres.h"
#include <ResultSet.h>
#include <Result.h>
#include <GenericException.h>
#include <MetaValue.h>
#include <stdlib.h>


using namespace DBAPI;
using std::string;
extern "C" {
	DBAPI::DatabaseBackend* _DatabaseEntry(const char *dsn) {
		string dsn2 = dsn;
		return new DatabaseBackendPostgres(dsn2);
	}
}

DatabaseBackendPostgres::DatabaseBackendPostgres(std::string dsn) {
	_dsn = dsn;
	_conn = new pqxx::connection(_dsn);
}

DatabaseBackendPostgres::~DatabaseBackendPostgres() {
	delete _transaction;
	delete _conn;
}

void DatabaseBackendPostgres::setQuery(std::string sql) {
	_sql = sql;
}
void DatabaseBackendPostgres::setQuery(char* sql) {
	_sql = sql;
}
std::string DatabaseBackendPostgres::escape(std::string value) {
	return _conn->esc(value);
}
char* DatabaseBackendPostgres::escape(char* value) {
	return (char*)_conn->esc(value).c_str();
}
bool DatabaseBackendPostgres::execute() {
	if (_sql == "") {
		_error = "No SQL Statement Defined";
		_errorNum = -2;
		return false;
	}
	_transaction = new pqxx::work(*_conn, "DBAPI Transaction");
	_result = _transaction->exec(_sql);
	return true;
}
DBAPI::ResultSet* DatabaseBackendPostgres::getResultSet() {
	int i, j;
	int numRows, numCols;
	ResultSet *rs;
	Result *r;
	MetaValue *m;


	bool vb;
	std::string vc;
	int vi;

	numRows = _result.size();
	numCols = _result.columns();

	rs = new ResultSet;
	rs->_results.resize(numRows);
	for (i=0; i < numRows; i++) {
		r = new Result();
		for (j=0;j<numCols;j++) {
			switch(_result[i][j].type()) {
				case 16:
					//Bool
					vb = _result[i][j].as(vb);
					r->addValue(new MetaValue(vb));
					break;

				case 18:
				case 25:
				case 1043:
					//Char
					vc = _result[i][j].as(vc);
					r->addValue(new MetaValue(vc));
					break;
				case 21:
				case 23:
				case 20:
					//Int
					vi = _result[i][j].as(vi);
					r->addValue(new MetaValue(vi));
				}
		}
		//Add result to resultset
		rs->_results.at(i) = r;
	}
	return rs;
}

string DatabaseBackendPostgres::getError() {
	return _error;
}

int DatabaseBackendPostgres::getErrorNum() {
	return _errorNum;
}
