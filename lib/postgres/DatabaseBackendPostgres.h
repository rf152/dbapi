/**
 * @file DatabaseBackendPostgres.h
 */
#ifndef DATABASEBACKENDPOSTGRES_H_
#define DATABASEBACKENDPOSTGRES_H_

#include <DBAPI.h>
#include <string>
#include <pqxx/connection.hxx>
#include <pqxx/transaction.hxx>
#include <DatabaseBackend.h>
/**
 * @brief PostgreSQL implementation of Database
 *
 */
class DBAPI::DatabaseBackendPostgres : public DBAPI::DatabaseBackend {
public:
	DatabaseBackendPostgres(std::string dsn);
	virtual ~DatabaseBackendPostgres();
	void setQuery(std::string sql);
	void setQuery(char* sql);
	std::string escape(std::string value);
	char* escape(char* value);
	bool execute();
	DBAPI::ResultSet* getResultSet();
	std::string getError();
	int getErrorNum();

private:
	std::string _dsn;
	pqxx::connection *_conn;
	pqxx::transaction<> *_transaction;
	pqxx::result _result;
};

#endif //DATABASEBACKENDPOSTGRES_H_
