/**
 * @file BackendNotFoundException
 */
#ifndef BACKENDNOTFOUNDEXCEPTION_H_
#define BACKENDNOTFOUNDEXCEPTION_H_

#include "DBAPI.h"
#include "GenericException.h"

/**
 * @brief BackendNotFoundException
 * Exception raised when the specified backend cannot be found
 */
class DBAPI::BackendNotFoundException : public DBAPI::GenericException {
public:
	BackendNotFoundException(const char* requestedType);
	virtual ~BackendNotFoundException() throw();

	const char* what() const throw();
private:
	const char* _type;
};

#endif /* BACKENDNOTFOUNDEXCEPTION_H_ */
