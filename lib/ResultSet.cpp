#include "ResultSet.h"
#include "Result.h"
#include "GenericException.h"

using namespace DBAPI;

ResultSet::ResultSet() {
	_iterator = 0;
	_doNext = false;
}

ResultSet::~ResultSet() {
	_results.clear();
}

bool ResultSet::next() {
	/*
	 * This is a massive bodge.
	 * It is basically to allow you to use a while(rs->next()) loop to loop
	 * through the values.
	 */

	if (!_doNext) {
		_doNext = true;
		return true;
	}
	if (_iterator == _results.size() - 1) {
		return false;
	}
	_iterator++;
	return true;
}

int ResultSet::rowCount() {
	return _results.size();
}

Result* ResultSet::getNextReult() {
	next();
	return getResult();
}

Result* ResultSet::getResult() {
	if (!_doNext) _doNext = true;
	if (_iterator == _results.size()) {
		throw GenericException("Row out of bounds");
	}
	return _results.at(_iterator);
}
