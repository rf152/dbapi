#include "MetaValue.h"

using DBAPI::MetaValue;

MetaValue::MetaValue(int value) {
	type = TYPE_INT;
	iValue = value;
}
MetaValue::MetaValue(char* value) {
	type = TYPE_CHAR;
	cValue = value;
}
MetaValue::MetaValue(std::string value) {
	type = TYPE_CHAR;
	cValue = (char*)value.c_str();
}
MetaValue::MetaValue(bool value) {
	type = TYPE_BOOL;
	bValue = value;
}
MetaValue::MetaValue(double value) {
	type = TYPE_DOUBLE;
	dValue = value;
}

MetaValue::~MetaValue() {

}
