/**
 * @file MetaValue.h
 */
#ifndef METAVALUE_H_
#define METAVALUE_H_

#include "DBAPI.h"
#include <string>

/**
 * @brief MetaValue
 * A MetaValue class to hold entries in a Result. This allows one class for
 * multiple data types
 */
class DBAPI::MetaValue {
public:
	MetaValue(int value);
	MetaValue(char* value);
	MetaValue(std::string value);
	MetaValue(bool value);
	MetaValue(double value);
	~MetaValue();
protected:
	int iValue;
	char* cValue;
	bool bValue;
	double dValue;
	DBAPI::COL_TYPE type;
	friend class DBAPI::Result;
};

#endif /* METAVALUE_H_ */
