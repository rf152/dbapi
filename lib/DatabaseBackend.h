/**
 * @file DatabaseGeneric.h
 */
#ifndef DATABASEBACKEND_H_
#define DATABASEBACKEND_H_

#include "DBAPI.h"
#include <string>

/**
 * @brief Generic class for backends to inherit
 */
class DBAPI::DatabaseBackend {
public:
	///Constructor
	DatabaseBackend();
	///Destructor
	virtual ~DatabaseBackend();
	/**
	 * Set Query (string)
	 * @param sql The SQL Query
	 */
	virtual void setQuery(std::string sql)=0;
	/**
	 * Set Query (char*)
	 * @param sql The SQL Query
	 */
	virtual void setQuery(char* sql)=0;
	/**
	 * Escape a string value
	 * @param value The value to be escaped
	 * @return The escaped value
	 */
	virtual std::string escape(std::string value)=0;
	/**
	 * Escape a char* value
	 * @param value The value to be escaped
	 * @return The escaped value
	 */
	virtual char* escape(char* value)=0;
	/**
	 * Run the stored query
	 * @return True if successful, false on error
	 */
	virtual bool execute()=0;
	/**
	 * Get the resultset associated with the most recent query
	 * @return ResultSet
	 */
	virtual DBAPI::ResultSet* getResultSet()=0;
	/**
	 * Get the error text from the most recent error
	 * @return error text
	 */
	virtual std::string getError()=0;
	/**
	 * Get the error number from the most recent error
	 * @return error number
	 */
	virtual int getErrorNum()=0;

protected:
	/// The stored SQL statement
	std::string _sql;
	/// The latest error
	std::string _error;
	/// The latest error number
	int _errorNum;
};

#endif /* DATABASEBACKEND_H_ */
