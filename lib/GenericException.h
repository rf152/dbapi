/**
 * @file GenericException.h
 */
#ifndef GENERICEXCEPTION_H_
#define GENERICEXCEPTION_H_

#include "DBAPI.h"
#include <exception>

class DBAPI::GenericException : public std::exception {
public:
	GenericException(const char* description);

	virtual const char* what() const throw();
protected:
	GenericException();
	const char* _description;
};

#endif /* GENERICEXCEPTION_H_ */
