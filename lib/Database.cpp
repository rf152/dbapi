#include "Database.h"
#include "DatabaseBackend.h"
#include "BackendNotFoundException.h"
#include <dlfcn.h>
#include <stdio.h>

#include <iostream>
using std::cerr;
using std::cout;
using std::endl;

using namespace DBAPI;
using std::string;

typedef DatabaseBackend * (*DatabaseSO_Entry)(const char*);

Database::Database(string dbType, string dsn) {
	_type = dbType;
	_setup((char*)dsn.c_str());
}

Database::Database(char* dbType, char* dsn) {
	_type = dbType;
	_setup(dsn);
}

Database::~Database() {
	if (_impl) {
		delete(_impl);
	}
	if (handle) {
		dlclose(handle);
	}
}

void Database::_setup(char* dsn) {
	cout << "Settings:" << endl <<
			" type" << _type << endl <<
			" dsn" << dsn << endl;
	char* path = new char();
	sprintf(path, "libdbapi-%s.so", _type.c_str());
	void* handle = dlopen(path, RTLD_NOW);
	if (!handle) {
		cerr << dlerror() << endl;
		throw BackendNotFoundException(_type.c_str());
	}
	DatabaseSO_Entry entry = (DatabaseSO_Entry)dlsym(handle, "_DatabaseEntry");

	if (!entry) {
		dlclose(handle);
		throw GenericException("No Entrypoint found in module");
	}

	_impl = entry(dsn);

	if (!_impl) {
		dlclose(handle);
		throw GenericException("No implementation returned");
	}
}

void Database::setQuery(std::string sql) {
	_impl->setQuery(sql);
}
void Database::setQuery(char* sql) {
	_impl->setQuery(sql);
}
std::string Database::escape(std::string value) {
	return _impl->escape(value);
}
char* Database::escape(char* value) {
	return _impl->escape(value);
}
bool Database::execute() {
	return _impl->execute();
}
std::string Database::getError() {
	return _impl->getError();
}
int Database::getErrorNum() {
	return _impl->getErrorNum();

}
ResultSet* Database::getResultSet() {
	return _impl->getResultSet();
}
