#include "GenericException.h"
#include <stdio.h>
using DBAPI::GenericException;


GenericException::GenericException(const char* description) {
	_description = description;
}
GenericException::GenericException() {
	_description = "I have no more information for you";
}

const char* GenericException::what() const throw() {
	char *error;
	sprintf(error, "An Error Occured: %s", _description);
	return (const char*)error;
}
