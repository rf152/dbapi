#include "Result.h"
#include "ColumnOutOfBoundsException.h"
#include "MetaValue.h"
using namespace DBAPI;

Result::Result() {
	//_values = new std::vector<DBAPI::MetaValue*>();
}
Result::~Result() {
	_values.clear();
}

COL_TYPE Result::columnType(int colID) {
	_sanity(colID);
	return _values.at(colID)->type;
}

std::string Result::getColumnString(int colID) {
	_sanity(colID);
	std::string *ret;
	ret = new std::string(_values.at(colID)->cValue);
	return *ret;
}

char* Result::getColumnChar(int colID) {
	_sanity(colID);
	return _values.at(colID)->cValue;
}

int Result::getColumnInt(int colID) {
	_sanity(colID);
	return _values.at(colID)->iValue;
}

double Result::getColumnDouble(int colID) {
	_sanity(colID);
	return _values.at(colID)->dValue;
}

long Result::getColumnLong(int colID) {
	_sanity(colID);
	return _values.at(colID)->iValue;
}

bool Result::getColumnBool(int colID) {
	_sanity(colID);
	return _values.at(colID)->bValue;
}

void Result::_sanity(int colID) {
	if (colID < 0) {
		throw ColumnOutOfBoundsException(colID);
	}
	if (colID > _values.size()) {
		throw ColumnOutOfBoundsException(colID);
	}
}

void Result::addValue(MetaValue* value) {
	_values.push_back(value);
}
