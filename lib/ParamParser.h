/**
 * @file ParamParser.h
 * @author Richard
 */
#ifndef PARAMPARSER_H_
#define PARAMPARSER_H_

#include "DBAPI.h"
#include <string>
#include <map>

class DBAPI::ParamParser {
public:
	/**
	 * Constructor
	 * @return
	 */
	ParamParser();
	/**
	 * Fetch a key from the paramList
	 * @param key The key to be fetched
	 * @param def The default value
	 * @return The value
	 */
	std::string getParam(std::string key, std::string def="");
	/**
	 * Parse a given param string.
	 * @param paramString
	 */
	void parse(std::string paramString);
private:
	std::map<std::string, std::string> _params;
	typedef std::pair<std::string, std::string> paramT;
};

#endif /* PARAMPARSER_H_ */
