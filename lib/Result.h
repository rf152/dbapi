/**
 * @file Result.h
 */
#ifndef RESULT_H_
#define RESULT_H_

#include "DBAPI.h"
#include <string>
#include <vector>
/**
 * @brief Result class
 * The Result class. This holds a vector of MetaValue pointers for the
 * values in this row.
 */
class DBAPI::Result {
public:
	Result();
	~Result();
	/// Get the type of a specified column
	DBAPI::COL_TYPE columnType(int colID);
	/// Get the character value of a column as a string
	std::string getColumnString(int colID);
	/// Get the character value of a column as a char* array
	char* getColumnChar(int colID);
	/// Get the integer value of a column as an int
	int getColumnInt(int colID);
	/// Get the double value of a column
	double getColumnDouble(int colID);
	/// Get the integer value of a column as a long
	long getColumnLong(int colID);
	/// Get the booolean value of a column
	bool getColumnBool(int colID);

	friend class DBAPI::DatabaseBackendPostgres;
	friend class DBAPI::DatabaseBackendMySQL;
protected:
	/// Function used by backends to add a value
	void addValue(MetaValue* value);
private:
	/// vector of column values
	std::vector<DBAPI::MetaValue*> _values;
	/// Check the column is within bounds
	void _sanity(int colID);
};

#endif /* RESULT_H_ */
