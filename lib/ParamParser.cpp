#include "ParamParser.h"

using DBAPI::ParamParser;
using std::string;

ParamParser::ParamParser() {

}

void ParamParser::parse(string paramString) {
	int i, j, previ;
	string sPair;
	paramT param;
	string key, value;

	i = 0;
	previ = 0;

	if (paramString == "") return;
	if (paramString.find("=") == paramString.npos) return;
	if (paramString.find(";") == paramString.npos) {
		//Single parameter only
		j = paramString.find("=");
		key = paramString.substr(0, j);
		value = paramString.substr(j+1);
		param = paramT(key, value);
		_params.insert(param);
	} else {
		while (i != paramString.npos) {
			i = paramString.find(";",previ);
			if (i == paramString.npos) {
				sPair = paramString.substr(previ);
			} else {
				sPair = paramString.substr(previ, i-previ);
			}
			previ = i+1;
			j = sPair.find("=");
			if (j == sPair.npos) continue;
			key = sPair.substr(0, j);
			value = sPair.substr(j+1);
			param = paramT(key, value);
			_params.insert(param);
		}
	}
}

string ParamParser::getParam(string key, string def) {
	if (_params.find(key) == _params.end()) return def;
	return _params[key];
}
