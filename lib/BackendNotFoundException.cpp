#include "BackendNotFoundException.h"
#include <stdio.h>

using namespace DBAPI;

BackendNotFoundException::BackendNotFoundException(const char* requestedType) {
	_type = requestedType;
}

BackendNotFoundException::~BackendNotFoundException() throw() {

}

const char* BackendNotFoundException::what() const throw() {
	char *error;

	sprintf(error, "The specified backend (%s) was not found", _type);

	return (const char*)error;
}
