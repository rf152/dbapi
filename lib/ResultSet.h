/**
 * @file ResultSet.h
 */
#ifndef RESULTSET_H_
#define RESULTSET_H_

#include "DBAPI.h"
#include <vector>
/**
 * @brief ResultSet class
 * The ResultSet class. This is the class that is used for accessing the
 * rows of a query's results.
 */
class DBAPI::ResultSet {
public:
	/// Constructor
	ResultSet();
	/// Destructor
	~ResultSet();
	/// Move forward to the next value
	bool next();
	/// Get a Result
	DBAPI::Result* getResult();
	/// Move forward, then get a result
	DBAPI::Result* getNextReult();
	/// Get the number of rows within a ResultSet
	int rowCount();

	friend class DBAPI::DatabaseBackend;
	friend class DBAPI::DatabaseBackendPostgres;
	friend class DBAPI::DatabaseBackendMySQL;

protected:
	/// Vector of Result Classes
	std::vector<DBAPI::Result*> _results;
	/// Counter for which result we are on
	int _iterator;
	/// Do we really want to move on (this is a bodge!)
	bool _doNext;

};

#endif /* RESULTSET_H_ */
