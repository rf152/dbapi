/**
 * @file Database.h
 */
#ifndef DATABASE_H_
#define DATABASE_H_

#include "DBAPI.h"
#include <string>
/**
 * @brief Database abstract class
 * The Database class which will be extended for each implementation
 * of Database.
 */
class DBAPI::Database {
public:
	Database(std::string dbType, std::string dsn);
	Database(char* dbType, char* dsn);
	virtual ~Database();
	virtual void setQuery(std::string sql);
	virtual void setQuery(char* sql);
	virtual std::string escape(std::string value);
	virtual char* escape(char* value);
	virtual bool execute();
	virtual DBAPI::ResultSet* getResultSet();
	std::string getError();
	int getErrorNum();

protected:
	/// Internal SQL storage
	std::string _sql;
	/// Most recent error number
	int _errorNum;
	/// Most recent error string;
	std::string _errorString;

private:
	/// Pointer to the specific implementation
	DBAPI::DatabaseBackend *_impl;
	/// Type string
	std::string _type;
	/// Unified setup function
	void _setup(char* dsn);
	/// Dynamic Loading Handle
	void *handle;

};

#endif /* DATABASE_H_ */
