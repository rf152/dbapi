/**
 * @file DBAPI.h
 * @brief Namespace declaration
 * Forward declaration of all aspects of the namespace.
 *
 */

namespace DBAPI {
	//Base Classes
	class Database;
	class ResultSet;
	class Result;
	class MetaValue;
	class ParamParser;

	//Database Wrapper
	class DatabaseBackend;

	//Postgres bindings
	class DatabaseBackendPostgres;
	//MySQL bindings
	class DatabaseBackendMySQL;

	//Exceptions
	class GenericException;
	class BackendNotFoundException;
	class ColumnOutOfBoundsException;

#ifndef COL_TYPE_ENUM
#define COL_TYPE_ENUM
	enum COL_TYPE {
		TYPE_BOOL,
		TYPE_INT,
		TYPE_DOUBLE,
		TYPE_CHAR,
		TYPE_BLOB
	};
#endif //COL_TYPE_ENUM
}
