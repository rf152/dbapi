#include <DBAPI.h>
#include <Database.h>
#include <Result.h>
#include <ResultSet.h>
#include <iostream>
#include <stdio.h>
#include <exception>
#include <GenericException.h>

using DBAPI::Database;
using DBAPI::ResultSet;
using DBAPI::Result;
using DBAPI::GenericException;
using namespace std;

int main(int argc, char** argv) {
	Database *db;
	try {
		db = new Database("mysql", "host=192.168.0.2;user=xop;password=5nKjfenzU3YabfZL;database=xop");
	} catch (GenericException e) {
		cout << "ERROR: " << e.what() << endl;
		return 0;
	}
	cout << "Database Opened" << endl;
	db->setQuery("SELECT * FROM operatingsystemversion WHERE osversionid=7");
	if (!db->execute()) {
		cout << "error" << endl;
		cout << db->getError() << endl;
	}
	cout << "Getting Resultset" << endl;
	ResultSet *rs = db->getResultSet();
	cout << "Got Resultset" << endl;
	Result *r;
	while (rs->next()) {
		cout << "Getting result" << endl;
		r = rs->getResult();
		cout << r->getColumnChar(3) << endl;
	}

	cout << db->escape("Richard's Chair") << endl;

	delete rs;

	db->setQuery("SELECT * FROM clientserver");
	rs = db->getResultSet();
	while(rs->next()) {
		cout << r->getColumnInt(0) << endl;
	}
	delete rs;


	cout << "Done with the db now!" << endl;
	return 0;
}
